packer {
  required_plugins {
    amazon = {
      version = "~> v1.1.4"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

# amazon-ebs Packer builder that is able to create Amazon AMIs backed by EBS volumes for use in EC2.
source "amazon-ebs" "my-app"{
ami_name      = "my-app-{{ timestamp }}"
instance_type = "t3.small"
region        = "us-east-1"
subnet_id     = "subnet-b266d7d4"

  #To find a base image (Ubuntu 22.04 LTS) for Packer in our region, we can use source_ami_filter.
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }

#based on your Linux distribution, specify the default user for Packer to ssh. For Ubuntu, it's an ubuntu user
ssh_username  = "ubuntu"

tags = {
    Name = "My App"
  }
}
#The build section
build {
  #For the source, select the amazon-ebs resource that we just created
  sources     = ["source.amazon-ebs.my-app"]

  #To copy local files from your machine where you run Packer, you can use file provisioner
  provisioner "file" {
    destination = "/tmp"
    source      = "files"
  }

  #To execute arbitrary commands inside the EC2, you can use a script or scripts
  provisioner "shell" {
    script      = "scripts/bootstrap.sh"
  }
}