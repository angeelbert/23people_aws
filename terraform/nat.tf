#To provide internet access for private subnets along with Elastic IP address
resource "aws_eip" "nat" {
  vpc = true

  tags = {
    Name = "nat"
  }
}

resource "aws_nat_gateway" "nat1" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public_us_east_1a.id

  tags = {
    Name = "nat1"
  }

  depends_on = [aws_internet_gateway.igw]
}

resource "aws_nat_gateway" "nat2" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public_us_east_1b.id

  tags = {
    Name = "nat2"
  }

  depends_on = [aws_internet_gateway.igw]
}