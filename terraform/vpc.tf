# Create a VPC would give us around 65000 IP addresses
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
  Name = "main"
  }
}