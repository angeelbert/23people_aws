//We will use a Gin web framework to build a simple REST api
package main

import (
    "log"
    "net/http"
    "os"
    "time"

    "github.com/gin-gonic/gin"
    "golang.org/x/sync/errgroup"
)

//To create two http web servers, we would need an errgroup.
var (
    g errgroup.Group
)

// let's create a getHostname handler for the /hostname endpoint. 
// It returns the hostname reported by the kernel of the node where the app is running.
// getHostname returns the host name reported by the kernel.
func getHostname(c *gin.Context) {
    name, err := os.Hostname()
    if err != nil {
        panic(err)
    }
    c.IndentedJSON(http.StatusOK, gin.H{"hostname": name})
}

// Then create another handler for the health check
// getHealthStatus returns the health status of your API.
func getHealthStatus(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{"status": "ready"})
}

// You can have an endpoint to quickly check the status of the app. It can be exposed to the internet
// ping quick check to verify API status.
func ping(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{"message": "pong"})
}

// we need to create two routes. The first main router will provide /hostname and /ping endpoint on port 8080.
func mainRouter() http.Handler {
    engine := gin.New()
    engine.Use(gin.Recovery())
    engine.GET("/hostname", getHostname)
    engine.GET("/ping", ping)
    return engine
}

// The job for the second router is to only serve the /health endpoint on port 8081.
func healthRouter() http.Handler {
    engine := gin.New()
    engine.Use(gin.Recovery())
    engine.GET("/health", getHealthStatus)
    return engine
}

// The main function, we need to initialize servers and associate them with corresponding routers.
// Then spin up goroutines for the main server and for the health server.
func main() {
    mainServer := &http.Server{
        Addr:         ":8080",
        Handler:      mainRouter(),
        ReadTimeout:  5 * time.Second,
        WriteTimeout: 10 * time.Second,
    }

    healthServer := &http.Server{
        Addr:         ":8081",
        Handler:      healthRouter(),
        ReadTimeout:  5 * time.Second,
        WriteTimeout: 10 * time.Second,
    }

    g.Go(func() error {
        err := mainServer.ListenAndServe()
        if err != nil && err != http.ErrServerClosed {
            log.Fatal(err)
        }
        return err
    })

    g.Go(func() error {
        err := healthServer.ListenAndServe()
        if err != nil && err != http.ErrServerClosed {
            log.Fatal(err)
        }
        return err
    })

    if err := g.Wait(); err != nil {
        log.Fatal(err)
    }
}